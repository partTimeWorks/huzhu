import Vue from 'vue'
import Router from 'vue-router'
import Home from 'pages/home'
import AccountManage from 'pages/accountManage'
import Financial from 'pages/financial'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/accountManage',
      name: 'accountManage',
      component: AccountManage
    },
    {
      path: '/financial',
      name: 'financial',
      component: Financial
    }
  ]
})
